MODULE rii

USE math

IMPLICIT NONE

INTEGER :: kmin, kmax,jl2,ju2
DOUBLE PRECISION, DIMENSION(:,:,:,:,:,:,:), ALLOCATABLE :: zc

CONTAINS

  SUBROUTINE main()
    INTEGER :: k,km,kp,q,mu2,ml2,mlp2

    jl2 = 1 ; ju2 = 3
    kmin=0 ;  kmax=2

  ALLOCATE(zc(-jl2:jl2,-jl2:jl2,-ju2:ju2,-ju2:ju2,kmin:kmax,kmin:kmax,-kmax:kmax))
  CALL main_loop()

END SUBROUTINE main

SUBROUTINE main_loop()
  INTEGER :: k,k2,kp,km,kp2,q,q2,mu2,mup2,ml2,mlp2,p2,pp2,ps2,pt2
  DOUBLE PRECISION :: z0, z1, z2, z3, z4, z5, z6, z7, zp
  
  zc=0.0
  !$acc data copyin(ju2,jl2,kmin,kmax) copy(zc)
  !$acc parallel vector_length(320)

  !$acc loop
  do k=kmin,kmax
     k2=2*k
     do kp=kmin,kmax 
        km = MIN(k,kp)
        kp2=2*kp
        z0=3.d0*dble(ju2+1)*dsqrt(dble(k2+1))*dsqrt(dble(kp2+1))
        do q=-km,km
           q2=2*q
           !$acc loop private(ml2,p2)
           do mu2=-ju2,ju2,2
              do ml2=-jl2,jl2,2
                 p2=mu2-ml2
                 if(iabs(p2).gt.2) cycle
                 PRINT*, "B ", ju2,jl2,mu2,-ml2,-p2
                 z1=w3js(ju2,jl2,2,mu2,-ml2,-p2)
                 do mup2=-ju2,ju2,2
                    if(mu2-mup2.ne.q2) cycle
                    pp2=mup2-ml2
                    if(iabs(pp2).gt.2) cycle
                    do mlp2=-jl2,jl2,2
                       ps2=mu2-mlp2
                       if(iabs(ps2).gt.2) cycle
                       pt2=mup2-mlp2
                       if(iabs(pt2).gt.2) cycle
                       zc(ml2,mlp2,mu2,mup2,k,kp,q)=mu2+ml2+mup2+mlp2+p2+pp2+ps2+pt2+ju2+jl2+z1
                    enddo
                 enddo
              enddo
           enddo
        end do
     end do
  end do
  

  !$acc end parallel
  !$acc end data

  write(1,*) zc

END SUBROUTINE main_loop
END MODULE rii
