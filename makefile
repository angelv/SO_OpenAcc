# Makefile

# Select compiler uncommenting the COMPILER and OPTIONS variables accordingly

#PGI
COMPILER = pgf90

# Intel Fortran compiler
#COMPILER = ifort -g -pg -traceback

# GFortran
#COMPILER = gfortran

OPTIONS = -c
#PROFILE = 
PROFILE = -g -pg

# OPTIONS = -c -frecord-marker=4
# DEBUG = -g -traceback -check noarg_temp_created


OBJECTS = math.o rii.o rii_main.o


redis: $(OBJECTS)
	   $(COMPILER) $(OBJECTS) $(PROFILE) -o rii_main.x

clean:
	find . -maxdepth 1 -name "*.o" -delete ; find . -maxdepth 1 -name "*.mod" -delete


math.o: math.f90
	$(COMPILER) $(OPTIONS) $(PROFILE) math.f90

rii.o: rii.f90 math.o 
	$(COMPILER) $(OPTIONS) $(PROFILE) rii.f90

rii_main.o: rii_main.f90 rii.o math.o
	$(COMPILER) $(OPTIONS) $(PROFILE) rii_main.f90
