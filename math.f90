MODULE math

IMPLICIT NONE

CONTAINS

FUNCTION W3JS(J1,J2,J3,M1,M2,M3)
  !$acc routine seq

  INTEGER, INTENT(IN) :: J1, J2, J3, M1, M2, M3
  DOUBLE PRECISION :: W3JS
  !-----------------------------------------------------------------------
  PRINT*, "I: ", J1,J2,M1,M2,M3
  W3JS = 0.12345 + dble(J1+J2+J3+M1+M2+M3)
  RETURN
END FUNCTION W3JS

END MODULE math
